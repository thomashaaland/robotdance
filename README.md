# RobotDance
A dance for a robot. The robot performs a dance for your amusement. The dance is detailed in custom_dance.ipynb. 

![](20221010_143803.mp4)

## License
Unlicensed
